import React from 'react';
import './Grocerie.css';
import{ FaPlusSquare} from 'react-icons/fa';

const grocerie=(props)=>
{
    let {click,index}=props;
   // console.log(props);
    return(
                <React.Fragment>
               
            
                <li className='gItem' onClick={()=>{click(index)}}><FaPlusSquare className='plus-icon'/> { props.name} </li>
                </React.Fragment>
    );
}

export default grocerie;