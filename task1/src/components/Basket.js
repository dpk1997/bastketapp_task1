import React from 'react';
import './Basket.css';
import{FaMinusSquare} from 'react-icons/fa';

const basket=(props)=>
{
    let {index,strike}=props;
    return(
        
    
            
                <li className={props.isCrossed?'strike bItem':'bItem'} onClick={()=>{strike(index)}}> <FaMinusSquare className='minus-icon'/> {props.count+' '+ props.name} </li>
                

    );

}

export default basket;