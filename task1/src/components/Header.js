import React from 'react';
import './Header.css';
import {FaShoppingBasket} from 'react-icons/fa'

const header = () => {
    return (
        <div className='App-header'>        
            <div className='App-title'>
                <header>
                    <FaShoppingBasket className='header-basket'/>
                <h1>Hello, Basket</h1>
                </header>
                
            </div>

        </div>
    );
}

export default header;