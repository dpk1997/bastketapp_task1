import React from "react";
import "./App.css";
import Header from "../components/Header.js";
import Grocerie from "../components/Grocerie.js";
import Basket from "../components/Basket";
import {FaLeaf,FaShoppingBasket,FaTrash} from 'react-icons/fa';
import Footer from '../components/Footer';
import Search from '../components/Search';


class App extends React.Component {
  state = {
    Groceries: [
      { id: 1, name: "Strawberry" },
      { id: 2, name: "Blueberry" },
      { id: 3, name: "Orange" },
      { id: 4, name: "Banana" },
      { id: 5, name: " Apple" },
      { id: 6, name: "Carrot" },
      { id: 7, name: "Celery" },
      { id: 8, name: "Mushroom" },
      { id: 9, name: "Green Pepper" },
      { id: 10, name: " Eggs" },
      { id: 11, name: "Cheese" },
      { id: 12, name: "Butter" },
      { id: 13, name: " Chicken" },
      { id: 14, name: " Beef" },
      { id: 15, name: "Pork" }
    ],
    Basket: [
      { id: 1, name: "Strawberry", count: 1, isCrossed: false },
      { id: 2, name: "Blueberry", count: 3 , isCrossed: false},
      { id: 3, name: "Orange", count: 2, isCrossed: false }
    ],
 
  };

  addToBasket = itemIndex => {
    let groItem = this.state.Groceries[itemIndex];

    let found = -1;
    for (let item in this.state.Basket) {
      if (this.state.Basket[item].id === groItem.id) {
        found = item;
        break;
      }
    }

    if (found != -1) {
      const Basket = [...this.state.Basket];
      Basket[found].count += 1;
     // Basket[found].isCrossed = !Basket[found].isCrossed;
      this.setState({
        Basket: Basket,
        message:null
      });
    } else {
      groItem.count = 1;
      const Basket = [...this.state.Basket];
      Basket.push(groItem);
      this.setState({
        Basket: Basket,
        message:null
      });
    }

  };

  deleteItem=()=>{
    console.log('sda');
    this.setState({
        Basket:[],
        message:"Your basket is empty!"
    });
  };

  strike=(itemIndex)=>
  {
   
      const Basket = [...this.state.Basket];  
      Basket[itemIndex].isCrossed = !Basket[itemIndex].isCrossed;
      this.setState({
        Basket: Basket,
        message:null
      });

  };



  render() {
    let items = this.state.Groceries.map((item, index) => {
      return (
        <Grocerie
          name={item.name}
          click={this.addToBasket}
          index={index}
          key={item.id}
        />
      );
    });

    let BasketItem = this.state.Basket.map((item, index) => {
      return <Basket name={item.name} count={item.count} isCrossed={item.isCrossed} key={item.id} index={index} strike={this.strike} />;
    });

    return (
      <div className="App">
        <Header />
      
        <nav>
          <Search/>
        </nav>
        
       
        <div className="Row">
          <div className="Grocerie">
          <header>
                    
                    <h3><FaLeaf/>Groceries</h3>
                </header>
            <ul>{items}</ul>
          </div>
          <div className="Basket">
          <h3><FaShoppingBasket/>Groceries
          <FaTrash className='delete' onClick={this.deleteItem}/>
          </h3>
          
            <ul>{BasketItem}</ul>
            <span>
              {this.state.message?this.state.message:null}
            </span>
          </div>
        </div>
        <Footer/>
      </div>
    );
  }
}

export default App;
